import React from "react";

class SalesRecord extends React.Component {
    constructor() {
        super();
        this.state = {
            recordlists: [],
        };
    }

    async componentDidMount() {
        const recordUrl = "http://localhost:8090/api/sales/";
        const response = await fetch(recordUrl);
        if (response.ok) {
            const data = await response.json();
            this.setState({ recordlists: data.recordlist });
        }
    }

    render() {
        return (
            <React.Fragment>
                <h1>Sales Log</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Customer</th>
                            <th scope="col">VIN</th>
                            <th scope="col">Sales Person</th>
                            <th scope="col">Sales Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.recordlists.map((recordlist, index) => {
                            return (
                                <tr key={index}>
                                    <td>{recordlist.customer}</td>
                                    <td>{recordlist.vin.vin}</td>
                                    <td>{recordlist.sales_person}</td>
                                    <td>{recordlist.sale_price}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </React.Fragment>
        );
    }
}

export default SalesRecord