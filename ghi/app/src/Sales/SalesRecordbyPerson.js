import React from 'react';

class SalesRecordbyPerson extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      employeeid: '',
      employees: [],
      specificemployee: [],
    };
    this.handleSalesRecordLogChange = this.handleSalesRecordLogChange.bind(this);
  }
  async componentDidMount() {
    const employeeurl = ('http://localhost:8090/api/sales/employees/');
    const response = await fetch(employeeurl);
      if (response.ok) {
        const data = await response.json();
        this.setState({employees: data.salesperson})
        console.log(data)
      }
  }
  handleSalesRecordLogChange(event) {
    const value = event.target.value;
    this.setState({employeeid: value})
    this.getEmployeeSales(value)
    }
  async getEmployeeSales(id){
    const specificurl = (`http://localhost:8090/api/sales/employees/${id}/`)
    const specificResponse = await fetch(specificurl);
        if (specificResponse.ok) {
            const specificData = await specificResponse.json();
            this.setState({specificemployee: specificData.recordlist})
        }
  }

  render () {
    return (
      <>
      <div className='container'>
        <h1>Sales Log by Employee</h1>
        <div>
        <select onChange={this.handleSalesRecordLogChange} required name="employeeid" id="employeeid" className="form-select">
                <option value="">Choose a Sales Person</option>
                {this.state.employees.map(sales_person => {
                  return (
                    <option key={sales_person.id} value={sales_person.id}>
                      {sales_person.name}
                    </option>
                  );
                })}
                </select>
        </div>
      </div>
      <div className='container'>
        <table className='table table-striped'>
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Customer</th>
              <th>Vin</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.specificemployee.map((recordlist, index) => {
                return(
                    <tr key={index}>
                        <td>{recordlist.sales_person}</td>
                        <td>{recordlist.customer}</td>
                        <td>{recordlist.vin.vin}</td>
                        <td>{recordlist.sale_price}</td>
                    </tr>
                )
            })}
          </tbody>
        </table>
      </div>
      </>
    )
  }
}



export default SalesRecordbyPerson;
