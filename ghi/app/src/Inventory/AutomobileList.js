import React from 'react';

class AutomobileList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        automobiles: []
    };
  }
  async componentDidMount() {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({automobiles: data.autos});
      }
  }
  render() {
    return (
      <>
        <h1>Automobile List</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>Year</th>
              <th>Color</th>
              <th>VIN</th>
            </tr>
          </thead>
          <tbody>
            {this.state.automobiles.map((auto, index) => {
              return (
                <tr key={index}>
                  <td>{auto.model.name}</td>
                  <td>{auto.model.manufacturer.name}</td>
                  <td>{auto.year}</td>
                  <td>{auto.color}</td>
                  <td>{auto.vin}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </>
    )
  }
}

export default AutomobileList;