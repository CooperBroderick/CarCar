from django.db import models

# Create your models here.
class VehicleVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, null=True, unique=True)

class Customer(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    phonenumber = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class SalesPerson(models.Model):
    name = models.CharField(max_length=50)
    employeeid = models.PositiveIntegerField((""))

    def __str__(self):
        return self.name

class SaleRecord(models.Model):
    sale_price = models.PositiveIntegerField((""))
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="salerecord",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="salerecord",
        on_delete=models.CASCADE,
    )
    vin = models.ForeignKey(
        VehicleVO,
        related_name="salerecord",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.customer