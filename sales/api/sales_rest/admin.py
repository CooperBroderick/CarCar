from django.contrib import admin
from .models import VehicleVO, Customer, SalesPerson, SaleRecord
# Register your models here.
admin.site.register(VehicleVO)
admin.site.register(Customer)
admin.site.register(SaleRecord)
admin.site.register(SalesPerson)