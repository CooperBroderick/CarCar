import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()


from sales_rest.models import VehicleVO

def poll():
    while True:
        print('Sales poller polling for data')
        response = requests.get("http://inventory-api:8000/api/automobiles/")
        content = json.loads(response.content)
        for vehicle in content["autos"]:
            VehicleVO.objects.update_or_create(
                import_href=vehicle["href"],
                defaults={"vin": vehicle["vin"]},
            )
        time.sleep(5)


if __name__ == "__main__":
    poll()

